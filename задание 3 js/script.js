const obj = {
  id:9587,
  uid:"eb3a3723-1340-4074-aa22-3a273af59478",
  blend_name:"Major Been",
  origin:"Atitlan, Guatemala",
  variety:"Red Bourbon",
  notes:"vibrant, juicy, cola, carmel, mango",
  intensifier:"unbalanced"
};

const card = document.querySelector(".card")
card.innerHTML = `
  <div>id: ${obj.id}</div>
  <div>uid: ${obj.uid}</div>
  <div>blend_name: ${obj.blend_name}</div>
  <div>origin: ${obj.origin}</div>
  <div>variety: ${obj.variety}</div>
  <div>notes: ${obj.notes}</div>
  <div>intensifier: ${obj.intensifier}</div>
  <button class="button">Сменить цвета</button>
`;

document.querySelector(".button").addEventListener('click', function() {
  card.classList.toggle('color-black');
  card.classList.toggle('background-grey');
})
